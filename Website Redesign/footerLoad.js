fetch('footer.html')
.then(res => res.text())
.then(text => {
    let oldelem = document.querySelector("script#replace_with_footer");
    let newelem = document.createElement("footer");
    newelem.id = "contact";
    newelem.classList.add('light-sec');
    newelem.classList.add('ta-c');
    newelem.classList.add('sec-vertical-pad');
    newelem.innerHTML = text;
    oldelem.parentNode.replaceChild(newelem,oldelem);
})