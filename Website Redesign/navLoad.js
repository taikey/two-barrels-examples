fetch('nav.html')
.then(res => res.text())
.then(text => {
    let oldelem = document.querySelector("script#replace_with_navbar");
    let newelem = document.createElement("nav");
    newelem.id = "nav";
    newelem.style.zIndex = 1001;
    newelem.innerHTML = text;
    oldelem.parentNode.replaceChild(newelem,oldelem);
})