let lastKnownScrollPosition = 0;
var gridPos,footerPos;
var topVid,button,cells,currentHover,currentProject;
var nav,navBar,sideNav;
var timeOuts = new Array();
let ticking = false;
let index;
var audio = new Audio('lightswitch.mp3');
var audio1 = new Audio('lightswitch2.mp3');
var hoveraudio = new Audio('sounds/hover2.wav');
var projectclick = new Audio('sounds/navigation_forward-selection-minimal.wav');
var projectback = new Audio('sounds/navigation_backward-selection-minimal.wav');
audioArray = [audio,audio1];
let x = 0;
let y = 0;
var targetX, currentX, targetY, currentY;
let increment = 0.9;
let windowWidth = window.innerWidth;
let windowHeight = window.innerHeight;
filters = ['convolve2','none'],
i = 0;

window.onresize = function(){
    updateVariables();
}

function updateVariables(){
    if(index){
        topVid = document.getElementById('head-video');
        button = document.getElementById('hover-fun');
        cells = document.getElementsByClassName('grid-cell');
        grid = document.getElementById('project-grid');
        gridPos = grid.getBoundingClientRect().top+window.pageYOffset;
        projectinfo = document.getElementsByClassName('project');
    }

    nav = document.getElementById('nav');
    navBar = document.getElementById('nav-bg');
    sideNav = document.getElementById('dot-nav');
    footer = document.getElementById('contact');
    footerPos = (document.body.scrollHeight-window.innerHeight);
    console.log(
        footer.getBoundingClientRect().top,
        window.pageYOffset,
        footer.getBoundingClientRect().height,
        footerPos,
        document.body.scrollHeight,
        "New Footer Pos:", (document.body.scrollHeight-window.innerHeight)
        );
}

function setWidths(){
    let e = document.getElementsByClassName('set-width');
    for (let i = 0; i < e.length; i++) {
        e[i].style.height = e[i].parentElement.getBoundingClientRect().height + "px";
        
    }
}

window.onload = function(){
    if(document.URL.includes("index.html") ){
        index=true;
    }
    updateVariables();
    setWidths();

    /*************EVENTS***************/
    document.addEventListener('scroll', function(e) {
        lastKnownScrollPosition = window.scrollY;
        
        if (!ticking) {
            window.requestAnimationFrame(function() {
                onScroll(lastKnownScrollPosition);
                ticking = false;
            });
        
            ticking = true;
        }
    });

    document.addEventListener('mousemove', e => {
        x = (e.clientX - (windowWidth/2)) * - 1;
        y = (e.clientY - (windowHeight/2)) * -1;

        if(lastKnownScrollPosition<=300){
            targetX = -5 + 0.5 * (x/(windowWidth/2));
            targetY = -5 + 0.5 * (y/(windowHeight/2));
        }
      //console.log(x, y,window.innerWidth,window.innerHeight);
    });

    //INDEX SPECIFIC EVENTS
    if(index){
        if(button)
            button.addEventListener('click',function(){
                audioArray[i].load();
                audioArray[i].play();
                if(topVid.classList.contains('video-fade')){
                    topVid.classList.remove('video-fade');
                }else{
                    topVid.classList.add('video-fade');
                }
                i++;
                if(i>=audioArray.length){
                    i=0;
                }
                
            }, false);

        
        for (let i = 0; i < cells.length; i++) {
            cells[i].addEventListener("mouseenter",function(e){
                currentHover = cells[i];
                var children = cells[i].querySelectorAll('.norm');
                if(children!=null){
                    for (let index = 0; index < children.length; index++) {
                        timeOuts[timeOuts.length] = removeClass(children[index],index,currentHover);
                    }
                }
                
            });
            
            cells[i].addEventListener("mouseleave",function(e){
                var children = cells[i].querySelectorAll('.reset');
                if(children!=null){
                    timeOuts.forEach(element => {
                        clearTimeout(element);
                    });
                    for (let index = 0; index < children.length; index++) {
                        children[index].classList.add('fadeout-bottom');
                        children[index].classList.remove('reset');
                    }
                }
            });

            cells[i].addEventListener("click",function(e){
                currentProject = i;
                projectclick.play();
                setTimeout(() => {window.location.href = cells[i].id},500);
                //window.location.href = 'about.html';
                grid.classList.add('fadeout-bottom');
                //document.getElementById('work').scrollIntoView();
                console.log(projectinfo[i]);
                //setTimeout(() => {projectinfo[i].classList.remove('fadeout-bottom')},500);
                //cells[i].style.width = "50%";
            });
        }
    }

}

function projectBack(){
    projectback.play();
    projectinfo[currentProject].classList.add('fadeout-bottom');
    setTimeout(() => {grid.classList.remove('fadeout-bottom')},500);
}

function removeClass(e,i,parent){
    var id = setTimeout(function(){
        if(parent == currentHover){
            e.classList.remove('fadeout-bottom');
            e.classList.add('reset');
        }
    },i*300)

    return id;
}
function sideNavCheck(pos){

    if(!document.getElementById('dot-nav')){
        return;
    }

    var bar = document.getElementById('dot-bar')
    var dotsArray = document.getElementsByClassName('dot');
    var textArray = document.getElementsByClassName('dot-text');

    //homepage vars
    var home2 = 600;
    var home3 = 2600;

    //about page vars
    var about2
    var about3
    var about4

    if ( document.URL.includes("index.html") ) {
        //console.log(sideNav.scrollTop);
        //Code here
        if(pos < gridPos-200){
            bar.style.top = "-5px";
            swapDot(0);
        }else if(pos < footerPos){
            bar.style.top = "44px";
            swapDot(1);
        }else{
            console.log("Bottom");
            bar.style.top = "92px";
            swapDot(2);
        }
        console.log(footerPos, pos)

        if(pos >= footerPos){
            sideNav.style.bottom = String(footer.getBoundingClientRect().height + 50) + "px";
            //sideNav.classList.add('dot-nav-override');
        }else{
            sideNav.style.bottom = null;
            //sideNav.classList.remove('dot-nav-override');
        }
    }else if (document.URL.includes("about.html") ){

    }

    function swapDot(dot){
        for (let i = 0; i < dotsArray.length; i++) {
            if(i!=dot){
                textArray[i].classList.remove('active');
            }else{
                textArray[i].classList.add('active');
            }
        }
    }
}

function onScroll(scrollPos) {
    // Do something with the scroll position
    //console.log(footer.getBoundingClientRect().top+window.pageYOffset, scrollPos);
      sideNavCheck(scrollPos);
      if(scrollPos <= 500){
          navBar.style.opacity = scrollPos/500;
      }else{
          navBar.style.boxShadow = "0 5px 15px 8px rgba(0,0,0,0.15)";
          navBar.style.opacity = 1;
      }
  }

function animSwap(elem) {
    document.getElementById(elem).style.animation = "animation 750ms linear both";
}