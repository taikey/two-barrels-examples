﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProceduralTree))]
[CanEditMultipleObjects]
public class ProceduralTreeInspector : Editor
{
    bool placeTree = false;
    public override void OnInspectorGUI()
    {
        ProceduralTree proceduralTree = (ProceduralTree)target;
        DrawDefaultInspector();
        
        if(GUILayout.Button("Generate Random Tree")){
            proceduralTree.treeSeed = Random.Range(1,999999);
            proceduralTree.GenerateTree();
        }

        if(GUILayout.Button("test")){
            placeTree = true;    
        }
    }

    private void OnSceneGUI() {
        ProceduralTree proceduralTree = (ProceduralTree)target;

        if(placeTree){
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));    
            if(Event.current.type == EventType.MouseDown){
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                RaycastHit hit = new RaycastHit();

                if (Physics.Raycast(ray, out hit, 1000.0f)) {
                    Vector3 newTreePosition = hit.point;
                    var tree = proceduralTree.DuplicateTree(newTreePosition);
                    tree.GetComponent<ProceduralTree>().treeSeed = Random.Range(1,999999);
                    tree.GetComponent<ProceduralTree>().GenerateTree();
                    tree.name = "Tree " +  tree.GetComponent<ProceduralTree>().treeSeed.ToString();
                }
                if(Event.current.modifiers != EventModifiers.Shift){
                    placeTree = false;
                }
            }
        }
    }
}