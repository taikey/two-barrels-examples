﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
[SelectionBase]
public class ProceduralTree : MonoBehaviour
{
    public Vector3 normalTest;
    public GenVars generationVariables;

    public TreeVars treeVariables;

    public TrunkVars trunkVariables;

    public BranchVars branchVariables;

    public SubBranchVars subBranchVariables;

    public LeafVars leafVariables;

    public RootVars rootVariables;

    public System.Random random;

    public int treeSeed;

    public bool isGenerated = false;

    private int meshCounter;
    private int uniqueCode;

    private bool locked = true;

    private TerrainChunk treeChunk;

    List<Vector3> gizmoSpheres;
    List<(Vector3,Vector3)> gizmoDirections;


    private void Start() {
        if(treeSeed == 0){
            treeSeed = Random.Range(1,999999);     
        }

        GenerateTree();
        locked = false;
    }

    private void OnValidate() {
        if(!locked)
        {
            UnityEditor.EditorApplication.delayCall += () =>
            {
                //GenerateObjects();
            };

            if(transform != null && !Application.isPlaying){
                GenerateTree();
            }
        }
    }

    public void GenerateTreeAwait(TerrainChunk chunk){
        GenerateTree();
        this.treeChunk = chunk;
    }

    public void GenerateTree(){
        gizmoSpheres = new List<Vector3>();
        gizmoDirections = new List<(Vector3,Vector3)>();
        uniqueCode = Random.Range(0, 9999999);
        meshCounter = 0;
        this.name = "Tree " + treeSeed.ToString();

        var obj = transform;
        var objPos = transform.position;
        var objUp = transform.up;

        if(!Application.isPlaying && !generationVariables.generateLODInEditor){
            random = new System.Random(treeSeed);
            
            ThreadedDataRequester.RequestData(() => ProceduralTreeThreadSafe.MainBranchGeneration(generationVariables,treeVariables,trunkVariables,branchVariables,subBranchVariables,leafVariables,rootVariables,random, uniqueCode, treeSeed, obj, objPos, objUp, 0),AssignMeshes);
        }else{
            for(int lod = 0;lod < generationVariables.lodLevels-1;lod++){
                random = new System.Random(treeSeed);

                //Debug.Log("Generating LOD " + lod + " With Seed " + treeSeed);

                //Generate the tree
                ThreadedDataRequester.RequestData(() => ProceduralTreeThreadSafe.MainBranchGeneration(generationVariables,treeVariables,trunkVariables,branchVariables,subBranchVariables,leafVariables,rootVariables,random, uniqueCode, treeSeed, obj, objPos, objUp, lod),AssignMeshes);  
                Thread.Sleep(10);
            }
        }
    }


    void AddMeshComponents(GameObject obj){
        obj.AddComponent<MeshFilter>();
        obj.AddComponent<MeshRenderer>();
    }

    void ParentTransform(Transform obj, Transform parent){
        obj.transform.parent = parent.transform;
        obj.transform.localPosition = Vector3.zero;
    }

    void AssignMeshes(object poop){
        var lodLevels = generationVariables.lodLevels;
        //var meshes = (((Vector3[],int[],Vector2[])[],int,int)) poop;
        var meshes = ((List<meshData[]> meshData,int lodLevel,int uniqueCode,branchStartVars branchStartVerts)) poop;
        gizmoSpheres = meshes.branchStartVerts.gizmoPositions;
        //var mesh = ((List<(List<Vector3>,List<int>,List<Vector2>)[]>,int,int)) poop;
        if(meshes.Item3 == uniqueCode){
            meshCounter += 1;
        }
        Debug.Log("Mesh count is " + meshCounter);


        var obj = transform.PartialFind("LOD " + meshes.Item2) ? transform.PartialFind("LOD " + meshes.Item2).gameObject : new GameObject(treeSeed + " LOD " + meshes.Item2);
        obj.name = treeSeed + " LOD " + meshes.Item2;
        obj.transform.ParentReset(transform);

        //Debug.Log("Creating " + obj.name);
        var body = obj.transform.Find("Body") ? obj.transform.Find("Body").gameObject : new GameObject("Body");
        if(body.transform.parent != obj)
            body.transform.ParentReset(obj.transform);
        
        //body.name = "Body" + (meshes.Item1[0].Item2.Length/3);
        var bodyMesh = meshes.meshData[0][0];//(meshes.Item1[0][0].verts.ToArray(),meshes.Item1[0][0].tris.ToArray(),meshes.Item1[0][0].uvs.ToArray());
        AddMesh(body,bodyMesh,treeVariables.treeMaterial,treeVariables.treeColor);

        if(body.GetComponent<MeshCollider>()==null)
        {
            var collider = body.AddComponent<MeshCollider>();
        }else
        {
            body.GetComponent<MeshCollider>().sharedMesh = body.GetComponent<MeshFilter>().sharedMesh;
        }


        if(generationVariables.generateBranches)
        { 
            for (int i = 0; i < meshes.meshData[1][0].verts.Length; i++)
            {
                var pos = meshes.meshData[1][0].verts[i];
                var dir = meshes.meshData[1][0].norms[i];
                gizmoDirections.Add((pos,dir));
            }

            for(int i = 0;i < meshes.meshData[1].Length;i++)
            {
                var branches = obj.transform.Find("Branches " + i) ? obj.transform.Find("Branches " + i).gameObject : new GameObject("Branches " + i);
                branches.gameObject.SetActive(true);
                var branchMesh = meshes.meshData[1][i];//(meshes.meshData[1][i].verts.ToArray(),meshes.Item1[1][i].tris.ToArray(),meshes.Item1[1][i].uvs.ToArray());

                if(branches.transform.parent != obj)
                    branches.transform.ParentReset(obj.transform);
            
                for(int x = 0; x < meshes.branchStartVerts.startVerts.Count; x++)
                {
                    var direction = -meshes.branchStartVerts.branchDirections[x];
                    var verts = meshes.branchStartVerts.startVerts[x];

                    for (int n = 0; n < verts.Length; n++)
                    {
                        var point = branchMesh.verts[verts[n]];
                        RaycastHit hit;
                        if(Physics.Raycast(transform.TransformPoint(point),direction,out hit,4))
                        {
                            //gizmoSpheres.Add(transform.TransformPoint(point));
                            //gizmoSpheres.Add(hit.point);
                            branchMesh.verts[verts[n]] = transform.InverseTransformPoint(hit.point);
                        }else if(Physics.Raycast(transform.TransformPoint(point),-direction,out hit,2))
                        {
                            branchMesh.verts[verts[n]] = transform.InverseTransformPoint(hit.point);
                        }else
                        {
                            Debug.Log("No hit");
                        }
                    }
                }
                
                AddMesh(branches.gameObject,branchMesh,branchVariables.material,branchVariables.color);
            }

            foreach(Transform branches in obj.transform.PartialFindAll("Branches"))
            {
                if(int.Parse(branches.name.Substring(branches.name.Length - 1)) >= meshes.Item1[1].Length)
                {
                    MyDestroy(branches.gameObject);
                }
            }
        }else
        {
            if(obj.transform.Find("Branches"))
            {
                MyDestroy(obj.transform.Find("Branches").gameObject);
            }
        }

        if(generationVariables.generateLeaves)
        {
            for(int i = 0;i < meshes.meshData[2].Length;i++)
            {
                var leaves = obj.transform.Find("Leaves " + i) ? obj.transform.Find("Leaves " + i).gameObject : new GameObject("Leaves " + i);
                var leafMesh = meshes.meshData[2][i];//(meshes.Item1[2][i].verts.ToArray(),meshes.Item1[2][i].tris.ToArray(),meshes.Item1[2][i].uvs.ToArray());

                if(leaves.transform.parent != obj)
                    leaves.transform.ParentReset(obj.transform);
                
                AddMesh(leaves.gameObject,leafMesh,leafVariables.material,leafVariables.color);
            }

            foreach(Transform leaves in obj.transform.PartialFindAll("Leaves"))
            {
                if(int.Parse(leaves.name.Substring(leaves.name.Length - 1)) >= meshes.Item1[2].Length)
                {
                    MyDestroy(leaves.gameObject);
                }
            }
        }else
        {
            foreach(Transform leaves in obj.transform.PartialFindAll("Leaves"))
            {
                MyDestroy(leaves.gameObject);
            }
        }
/*
        if(generationVariables.generateRoots){
            var roots = obj.transform.Find("Roots") ? obj.transform.Find("Roots").gameObject : new GameObject("Roots");
            if(roots.transform.parent != obj)
                roots.transform.ParentReset(obj.transform);

            //roots.name = "Roots" + (meshes.Item1[3].Item2.Length/3);
            AddMesh(roots,meshes.Item1[3],treeVariables.treeMaterial,treeVariables.treeColor);
        }else{
            if(obj.transform.Find("Roots")){
                MyDestroy(obj.transform.Find("Roots").gameObject);
            }
        }*/

        if(Application.isPlaying || generationVariables.generateLODInEditor){
            if(meshes.Item2 == 0 ){
                AddMeshCollider(body.GetComponent<MeshFilter>().sharedMesh,transform.gameObject,false);
                if(meshes.Item3 == uniqueCode){
                    meshCounter +=1;
                }

                //Debug.Log("Adding Billboard, Mesh count is " + meshCounter);
                var lastLOD = transform.PartialFind(" LOD " + (lodLevels-1)) ? transform.PartialFind(" LOD " + (lodLevels-1)).gameObject : new GameObject(treeSeed + " LOD " + (lodLevels-1));
                lastLOD.name = treeSeed + " LOD " + (lodLevels-1);

                if(lastLOD.transform.childCount > 0){
                    ClearChildren(lastLOD.transform);
                }

                if(lastLOD.transform.parent != transform)
                    lastLOD.transform.ParentReset(transform);

                GenerateBillboard(lastLOD.transform);
            }

            var finishCheck = meshCounter == lodLevels;

            if(finishCheck){
                foreach(Transform child in transform){
                    if(child.name.Contains("LOD")){
                        var lodLevel = int.Parse(child.name.Substring(child.name.Length - 1));

                        if(lodLevel > lodLevels-1){
                            MyDestroy(child.gameObject);
                        }
                    }
                }
                AssignLODGroups();
                isGenerated = true;

                if(treeChunk != null){
                    treeChunk.GenerateCopies();
                }
            }
        }
    }

    void ClearChildren(Transform obj){
        var length = obj.childCount;
        for (int i = 0; i < length; i++)
        {
            MyDestroy(obj.GetChild(0).gameObject);
        }
    }

    void AddMeshCollider(Mesh mesh, GameObject obj, bool isConvex){
        
        var collider = obj.GetComponent<MeshCollider>() ? obj.GetComponent<MeshCollider>() : obj.AddComponent<MeshCollider>();
        
        collider.sharedMesh = mesh;
        collider.convex = isConvex;
    }

    void AddMesh(GameObject obj, meshData mesh, Material mat, Color color){
        MeshFilter filter = obj.GetComponent<MeshFilter>() == null ?  obj.AddComponent<MeshFilter>() : obj.GetComponent<MeshFilter>();
        MeshRenderer rend = obj.GetComponent<MeshRenderer>() == null ? obj.AddComponent<MeshRenderer>() : obj.GetComponent<MeshRenderer>();

        if(mat != null){
            //mat.SetColor("_Color",color);
            //var tempMat = new Material(mat);
            //tempMat.color = color;
            rend.material = mat;
        }else{
            var tempMat = new Material(Shader.Find("Standard"));
            rend.material = tempMat;
        }

        filter.mesh = new Mesh();
        filter.sharedMesh.vertices = mesh.verts;
        filter.sharedMesh.triangles = mesh.tris;
        filter.sharedMesh.uv = mesh.uvs;
        filter.sharedMesh.RecalculateBounds();

        if(mesh.norms!=null)
        {
            filter.sharedMesh.normals = mesh.norms;
        }
        else
        {
            filter.sharedMesh.RecalculateNormals();
        }
        //filter.sharedMesh.RecalculateNormals();

    }

    public void AssignLODGroups(){
        var lodLevels = generationVariables.lodLevels;
        //Debug.Log("Assign LODs");
        LODGroup lodGroup;
        LOD[] lods = new LOD[lodLevels];
        if(this.GetComponent<LODGroup>()==null){
            lodGroup = gameObject.AddComponent<LODGroup>();
        }else{
            lodGroup = gameObject.GetComponent<LODGroup>();
        }

        foreach(Transform child in transform){
            if(child.name.Contains("LOD")){
                Renderer[] renderers = new Renderer[child.childCount+1];
                renderers[0] = child.gameObject.GetComponent<Renderer>();

                for (int i = 0; i < child.childCount; i++)
                {
                    renderers[i+1] = child.GetChild(i).GetComponent<Renderer>();
                }
                
                int objLodLevel = int.Parse(child.name.Substring(child.name.Length - 1));
                if(objLodLevel==0){
                    lods[objLodLevel] = new LOD(0.95F, renderers);
                }else if(objLodLevel < lodLevels-1){
                    lods[objLodLevel] = new LOD(0.95F-((0.87F/(lodLevels-2))*objLodLevel), renderers);
                }else{
                    lods[objLodLevel] = new LOD(0.01F, renderers);
                }
            }
        }
        lodGroup.SetLODs(lods);
        lodGroup.RecalculateBounds();
    }

    void GenerateFalseBranchObject(){
        var branch = new GameObject("BillboardBranch");
        var leaves = new GameObject("BillboardLeaves");

        branch.transform.ParentReset(transform);
        leaves.transform.ParentReset(branch.transform);
        List<(Vector3,Vector3,float,float)> subBranchVars = new List<(Vector3, Vector3, float, float)>();

        List<Vector3> leafVerts = new List<Vector3>();
        List<int> leafTris = new List<int>();
        List<Vector2> leafUVs = new List<Vector2>();

        //ProceduralTreeThreadSafe.GenerateBranch()

    }
    void GenerateBranchBillboard(){
        var cameraObj = new GameObject("BillboardCamera");
        cameraObj.transform.position = transform.position;
        cameraObj.transform.parent = transform;

        var camera = cameraObj.AddComponent<Camera>();
        var billboardTexture = new RenderTexture(512,512,0,RenderTextureFormat.ARGB32);
        camera.targetTexture = billboardTexture;
        camera.orthographic = true;
        camera.cullingMask = 1 << 22;
        camera.targetDisplay = 0;
        camera.clearFlags = CameraClearFlags.SolidColor;
        camera.backgroundColor = new Color(0,0,0,0);
        camera.farClipPlane = 100;
        
        var tree = transform.Find(treeSeed + " LOD 0");
        var oldLayer = tree.gameObject.layer;
        foreach(Transform child in tree){
            child.gameObject.layer = 22;
        }

        var combinedBounds = tree.GetChild(0).GetComponent<MeshFilter>().sharedMesh.bounds;
        var renderers = tree.GetComponentsInChildren<MeshFilter>();

        foreach(MeshFilter render in renderers) {
            combinedBounds.Encapsulate(render.sharedMesh.bounds);
        }

        var maxBound = Mathf.Abs(combinedBounds.extents.x)+Mathf.Abs(combinedBounds.center.x) > combinedBounds.extents.y ? (Mathf.Abs(combinedBounds.extents.x)+Mathf.Abs(combinedBounds.center.x)) :  combinedBounds.extents.y;

        camera.farClipPlane = (combinedBounds.extents.z*2)+1;
        cameraObj.transform.localPosition = new Vector3(0,maxBound,combinedBounds.center.z-(combinedBounds.extents.z+0.5f));
        camera.orthographicSize = maxBound;

        RenderTexture.active = billboardTexture;
        camera.Render();

        var texture = new Texture2D(billboardTexture.width,billboardTexture.height,TextureFormat.RGBA32,false);
        var material = new Material(Shader.Find("Custom/Billboard"));
        texture.ReadPixels(new Rect(0,0,billboardTexture.width,billboardTexture.height), 0, 0);
        texture.Apply(false);  
        texture.EncodeToPNG();
        //byte[] bytes = texture.EncodeToPNG();

        List<Material> oldMat = new List<Material>();
        var normalMat = new Material(Shader.Find("Custom/DisplayNormals"));
        foreach(Transform child in tree){
            var childRend = child.GetComponent<MeshRenderer>();
            oldMat.Add(childRend.sharedMaterial);
            childRend.material = null;
            childRend.material = normalMat;
        }
        camera.Render();
        
        var normal = new Texture2D(billboardTexture.width,billboardTexture.height,TextureFormat.ARGB32,false);
        normal.ReadPixels(new Rect(0,0,billboardTexture.width,billboardTexture.height), 0, 0);
        normal.Apply(false);
        normal.EncodeToPNG();

        var i = 0;
        foreach(Transform child in tree){
            var childRend = child.GetComponent<MeshRenderer>();
            childRend.material = oldMat[i];
            i++;
        }

        material.SetTexture("_MainTex",texture);
        material.SetTexture("_BumpMap",normal);
        material.SetFloat("_AlphaClip",0.5f);
        material.mainTextureScale = new Vector2(1,0.99f);
        
        if(tree != null){
            foreach(Transform child in tree){
                child.gameObject.layer = oldLayer;
            }
        }

        RenderTexture.active = null;
        camera.targetTexture = null;
        MyDestroy(cameraObj);
    }

    void GenerateBillboard(Transform parent){
        var billboardObj = GameObject.CreatePrimitive(PrimitiveType.Quad);
        MyDestroy(billboardObj.GetComponent<MeshCollider>());
        billboardObj.transform.ParentReset(parent);
        billboardObj.name = "Billboard";
        
        var cameraObj = new GameObject("BillboardCamera");
        cameraObj.transform.position = transform.position;
        cameraObj.transform.parent = transform;

        var camera = cameraObj.AddComponent<Camera>();
        var billboardTexture = new RenderTexture(512,512,0,RenderTextureFormat.ARGB32);
        camera.targetTexture = billboardTexture;
        camera.orthographic = true;
        camera.cullingMask = 1 << 22;
        camera.targetDisplay = 0;
        camera.clearFlags = CameraClearFlags.SolidColor;
        camera.backgroundColor = new Color(0,0,0,0);
        camera.farClipPlane = 100;
        
        var tree = transform.Find(treeSeed + " LOD 0");
        var oldLayer = tree.gameObject.layer;
        foreach(Transform child in tree){
            child.gameObject.layer = 22;
        }

        var combinedBounds = tree.GetChild(0).GetComponent<MeshFilter>().sharedMesh.bounds;
        var renderers = tree.GetComponentsInChildren<MeshFilter>();

        foreach(MeshFilter render in renderers) {
            combinedBounds.Encapsulate(render.sharedMesh.bounds);
        }

        var maxBound = Mathf.Abs(combinedBounds.extents.x)+Mathf.Abs(combinedBounds.center.x) > combinedBounds.extents.y ? (Mathf.Abs(combinedBounds.extents.x)+Mathf.Abs(combinedBounds.center.x)) :  combinedBounds.extents.y;

        camera.farClipPlane = (combinedBounds.extents.z*2)+1;
        cameraObj.transform.localPosition = new Vector3(0,maxBound,combinedBounds.center.z-(combinedBounds.extents.z+0.5f));
        camera.orthographicSize = maxBound;
        billboardObj.transform.localScale = billboardObj.transform.localScale * maxBound*2;
        billboardObj.transform.localPosition = new Vector3(0,maxBound,0);

        RenderTexture.active = billboardTexture;
        camera.Render();

        var texture = new Texture2D(billboardTexture.width,billboardTexture.height,TextureFormat.RGBA32,false);
        var material = new Material(Shader.Find("Custom/Billboard"));
        texture.ReadPixels(new Rect(0,0,billboardTexture.width,billboardTexture.height), 0, 0);
        texture.Apply(false);
        texture.EncodeToPNG();
        //byte[] bytes = texture.EncodeToPNG();

        List<Material> oldMat = new List<Material>();
        var normalMat = new Material(Shader.Find("Custom/DisplayNormals"));
        foreach(Transform child in tree){
            var childRend = child.GetComponent<MeshRenderer>();
            oldMat.Add(childRend.sharedMaterial);
            childRend.material = null;
            childRend.material = normalMat;
        }
        camera.Render();
        
        var normal = new Texture2D(billboardTexture.width,billboardTexture.height,TextureFormat.ARGB32,false);
        normal.ReadPixels(new Rect(0,0,billboardTexture.width,billboardTexture.height), 0, 0);
        normal.Apply(false);
        normal.EncodeToPNG();

        var i = 0;
        foreach(Transform child in tree){
            var childRend = child.GetComponent<MeshRenderer>();
            childRend.material = oldMat[i];
            i++;
        }


        material.SetTexture("_MainTex",texture);
        material.SetTexture("_BumpMap",normal);
        material.SetFloat("_AlphaClip",0.5f);
        material.mainTextureScale = new Vector2(1,0.99f);
        billboardObj.GetComponent<MeshRenderer>().material = material;
        
        if(tree != null){
            foreach(Transform child in tree){
                child.gameObject.layer = oldLayer;
            }
        }

        RenderTexture.active = null;
        camera.targetTexture = null;
        MyDestroy(cameraObj);


        //File.WriteAllBytes(Application.dataPath + "/Materials-Textures-Shaders/Textures/SavedScreen.png", bytes);
        //AssetDatabase.Refresh();

    }

    private void MyDestroy(GameObject obj){
        if(Application.isPlaying){
            Destroy(obj);
        }else{
            DestroyImmediate(obj);
        }
    }

    private void MyDestroy(Object obj){
        if(Application.isPlaying){
            Destroy(obj);
        }else{
            DestroyImmediate(obj);
        }
    }
    
    public Vector3 GetCenter(Vector3[] verts,int loopVertCount, int currentLoop){
        var startVertIndex = currentLoop * (loopVertCount+1);
        var center = verts[ startVertIndex ] + ( verts[ startVertIndex + ( loopVertCount / 2 )] - verts[ startVertIndex ] ) / 2;
        return center;
    }

    void MergeMeshes(GameObject parent, bool regenUVs){
        MeshFilter[] meshFilters = parent.GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        Matrix4x4 parentPos = parent.transform.worldToLocalMatrix;
        for (int i = 0; i < meshFilters.Length; i++)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = parentPos * meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);
        }
        parent.GetComponent<MeshFilter>().sharedMesh = new Mesh();
        parent.GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combine);

        if(regenUVs){
            Vector2[] meshUV = new Vector2[parent.GetComponent<MeshFilter>().sharedMesh.vertices.Length];
            for (int i = 0; i < parent.GetComponent<MeshFilter>().sharedMesh.vertices.Length; i++)
            {
                meshUV[i] = new Vector2(parent.GetComponent<MeshFilter>().sharedMesh.vertices[i].x,parent.GetComponent<MeshFilter>().sharedMesh.vertices[i].z);
            }
            
            parent.GetComponent<MeshFilter>().sharedMesh.uv = meshUV;
        }
        parent.SetActive(true);
    }

    public GameObject DuplicateTree(Vector3 pos){
        var tree = Instantiate(this,pos,this.transform.rotation).gameObject;
        return tree;
    }

    private void OnDrawGizmos()
    {
        foreach(var item in gizmoSpheres)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.TransformPoint(item),0.25f);
        }

        foreach(var item in gizmoDirections)
        {
            Gizmos.color = Color.red;
            var pos1 = transform.TransformPoint(item.Item1);
            var pos2 = transform.TransformPoint(item.Item1) + item.Item2;
            Gizmos.DrawLine(pos1,pos2);
        }
    }
}
