using UnityEngine;

[System.Serializable]
public class LeafVars
{
    public Material material;

    [ColorUsageAttribute(true,true)] public Color color = Color.white;

    public float size = 1;

    [Range(0,1)]public float sizeRandomness = 0.25f;
    
    public int density = 2;

    [Range(0,1)] public float genStartPos = 0.25f;

    [Range(0,1)] public float branchDirectionAlignment = 1;

    [Range(0,1)] public float randomRoll = 1; 

    [MinMaxSlider(0,7)] public Vector2 leafGenPos = new Vector2(2,2);


}
