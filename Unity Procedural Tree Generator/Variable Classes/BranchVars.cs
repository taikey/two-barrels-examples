using UnityEngine;

[System.Serializable]
public class BranchVars
{
    public Material material;
    [ColorUsageAttribute(true,true)]
    public Color color = Color.white;
    public float width = 0.5f;
    [Range(1,200)]
    public float length = 5f;
    public AnimationCurve heightRelativeLength = new AnimationCurve(
        new Keyframe(0,1),
        new Keyframe(1,0)
    );
    [Range(1,12)]
    public int density = 3;
    [Range(4,24)]
    public int loopCuts = 5;
    [Range(3,24)]
    public int loopVertCount = 5;
    [Range(-1,1)]
    public float initialAngle = 0;

    public AnimationCurve heightRelativeAngle = new AnimationCurve(
        new Keyframe(0,0),
        new Keyframe(1,1)
    );

    [Range(-2,2)]
    public float droop = 0;
    [Range(0,1)]
    public float rigidity = 0;
    [Range(0,1)]
    public float straightness = 1;
    [Range(0,1)]
    public float crinkliness = 0;
    [Range(0,1)]
    public float genStartPosition = 0.25f;
    [Range(0,1)]
    public float heightRelativeWidth = 0;
    [Range(0,1)]
    public float endSharpness = 1;
    [Range(0,1)]
    public float splitChance = 0.5f;
    [Range(-1,1)]
    public float curl = 0;
    [Range(0.1f,1)]
    public float curlScale = 1;
    [Range(0,1)]
    public float taper = 1;

    public bool weld = true;

    public AnimationCurve valueCurve = new AnimationCurve(
        new Keyframe(0,0),
        new Keyframe(0.1f,0.25f),
        new Keyframe(0.75f,0.35f),
        new Keyframe(1,1)
    );
}
