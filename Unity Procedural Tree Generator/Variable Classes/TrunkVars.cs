using UnityEngine;

[System.Serializable]
public class TrunkVars
{
    public AnimationCurve rootShape = new AnimationCurve(
        new Keyframe(0,0),
        new Keyframe(0.17f,0.2f,2,2),
        new Keyframe(0.4f,0.8f,1.9f,1.9f),
        new Keyframe(0.45f,0.9f),
        new Keyframe(0.55f,1f),
        new Keyframe(0.65f,0.85f,-1.7f,-1.7f),
        new Keyframe(0.7f,0.6f,-1.7f,-1.7f),
        new Keyframe(1,0)
    );

    public AnimationCurve rootBlend = new AnimationCurve(
        new Keyframe(0,0),
        new Keyframe(0.15f,0.15f,1.9f,1.9f),
        new Keyframe(0.45f,0.8f,0.95f,0.95f),
        new Keyframe(1,1)
    );
    [Range(2,16)]
    public int trunkLoopCuts = 8;
    [Range(0.01f,0.9f)]
    public float trunkHeight = 0.25f;
}
