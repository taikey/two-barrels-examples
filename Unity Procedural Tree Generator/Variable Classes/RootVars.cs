using UnityEngine;

[System.Serializable]
public class RootVars
{
    [Range(1,24)] public int loopCuts = 5;

    [Range(1,24)] public int loopVertCount = 5;

    [Range(0f,50f)] public float length = 5;

    [Range(0f,1f)] public float width = 0.25f;
    
    [Range(0,1)] public float endSharpness = 1;

    [Range(0,1)] public float taper = 1;

    [Range(0,1)] public float splitChance = 0.5f;

    [MinMaxSlider(0f,1f)] public Vector2 rootGenPos = new Vector2(0.2f,0.5f);

    public int rootDensity = 1;

    [Min(0)] public float rootStraightness = 0.75f;

    [Range(-2f,2f)] public float rootDroop = -1;

    [Range(-1f,1f)] public float rootInitialAngle = 0;

    public int subDensity = 2;

    public int subLength = 2;

    [Range(0,1)] public float subDirectionAlignment = 0.5f;
}
