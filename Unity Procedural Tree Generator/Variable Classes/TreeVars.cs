using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TreeVars
{
    public int treeSeed;

    public Material treeMaterial;

    [ColorUsageAttribute(true,true)] public Color treeColor = Color.white;

    [Min(0.1f)] public float scale = 1;
    
    [Min(0)] public float treeWidth = 2;

    [Min(0)] public float treeHeight = 15;

    [Range(3,128)] public int treeLoopVertCount = 48;

    [Range(2,128)] public int treeLoopCuts = 12;

    [Range(0,1)] public float treeTaper = 1;

    [Range(0,1)] public float treePointSharpness = 0.25f;

    [Min(0)] public float treeStraightness = 0.25f;

    [Range(0,1)] public float treeBaseAngle = 0.25f;

    [Range(0,1)] public float treeBaseSpacing = 0.25f;

    [Range(0,1)] public float treeBaseHeightRandomness = 0.25f;

    public float treeTwist = 0.5f;

    [MinMaxSlider(1,10)] public Vector2 treeBaseCountSlide = new Vector2(1,1);
}
