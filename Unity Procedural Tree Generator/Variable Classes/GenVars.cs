using UnityEngine;

[System.Serializable]
public class GenVars
{
    public enum treeTypes
    {
        custom,
        random,
        pineTree,
        oakTree
    }

    public treeTypes treeType;

    [Range(2,8)] public int lodLevels = 3;

    public bool generateTrunk = true,generateTrunkFlares = true, generateBranches = false, generateSubBranches = false, generateLeaves = false, generateRoots = false, generateSubRoots,rays = false,highQualityMesh = true,generateLODInEditor = false; 
}
