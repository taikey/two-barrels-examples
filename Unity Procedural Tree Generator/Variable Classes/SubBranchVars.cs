using UnityEngine;

[System.Serializable]
public class SubBranchVars
{
    [Range(0,1)] public float length = 0.5f;

    public AnimationCurve heightRelativeLength = new AnimationCurve(
        new Keyframe(0,0),
        new Keyframe(1,1)
    );

    [Range(0,1)] public float width = 0.5f;

    [Range(1,5)] public int genSteps = 2;

    [Range(0,1)] public float branchDirectionAlignment = 0.5f;

    [Min(0)] public int density = 2;
}
