﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;



public static class ProceduralTreeThreadSafe
{
    public static Mesh GenerateMesh(bool highQualityMesh, Vector3[] verts, int[] tris, Vector2[] UVs = default(Vector2[]))
    {
        var mesh = new Mesh();
        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.RecalculateNormals();

        if (UVs != null)
            mesh.uv = UVs;

        /*
        if(highQualityMesh)
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;*/

        return mesh;
    }

    public static void MyDestroy(GameObject obj)
    {
        if (Application.isPlaying)
        {
            UnityEngine.GameObject.Destroy(obj);
        }
        else
        {
            UnityEngine.GameObject.DestroyImmediate(obj);
        }
    }

    public static void MyDestroy(Object obj)
    {
        if (Application.isPlaying)
        {
            UnityEngine.Object.Destroy(obj);
        }
        else
        {
            UnityEngine.Object.DestroyImmediate(obj);
        }
    }

    public static float RandomRange(System.Random source, float min, float max)
    {
        return (float)(source.NextDouble() * (max - min) + min);
    }

    //  Methods to generate mesh information
    public static (Vector3[] verts, Vector3[] normals, Vector3[] gizmoPositions) GenerateBranchVertices(Transform obj, int loopCuts, int loopVertCount, float[] width, bool isMainBranch, Vector3[] refPoints, float taper, bool blend, Vector3 origin = default(Vector3), Vector3 parentDirection = default(Vector3))
    {
        loopVertCount += 1;
        int branchBlend = blend ? 3 : 0;
        Vector3[] vertices = new Vector3[((loopCuts + branchBlend - 1) * loopVertCount) + 1];
        Vector3[] normals = new Vector3[vertices.Length];
        Vector3[] startVerts = new Vector3[loopVertCount];
        List<Vector3> gizmoPositions = new List<Vector3>();
        Vector3 midpoint = Vector3.zero;
        var vert = 0;
        var lastDir = Vector3.up;

        //Loopcut Loop
        for (int i = 0; i < loopCuts; i++)
        {
            var lastIndex = loopCuts - 1;
            float angle = 0f;

            if (i < lastIndex)   //Checking if loop is on last loop
            {
                //Position variables
                var previousPoint = i > 0 ? refPoints[i - 1] : Vector3.zero;
                var currentPoint = refPoints[i];
                var nextPoint = refPoints[i + 1];

                //Direction variables
                var direction = nextPoint - currentPoint;
                var halfDirection = i > 0 ? ((currentPoint - previousPoint).normalized + (nextPoint - currentPoint).normalized).normalized : direction.normalized;
                var cross = halfDirection == Vector3.up ? Vector3.forward : Vector3.up;
                var tan = Vector3.Cross(halfDirection, cross).normalized;

                for (int vc = 0; vc < loopVertCount; vc++)
                {
                    var position = currentPoint + (Quaternion.AngleAxis(((float)vc / (loopVertCount - 1) * 360f) + angle, halfDirection) * tan) * (width[i] / 2);
                    var lastVIndex = loopVertCount - 1;

                    if (vc == 0 && i > 0)
                    {
                        var point = vertices[vert - loopVertCount] + (refPoints[i] - refPoints[i - 1]);
                        var positive = currentPoint + (Quaternion.AngleAxis(1, halfDirection) * tan) * (width[i] / 2);
                        var negative = currentPoint + (Quaternion.AngleAxis(-1, halfDirection) * tan) * (width[i] / 2);
                        var positiveBool = Vector3.Distance(positive, point) < Vector3.Distance(negative, point) ? true : false;
                        float oldDistance;
                        float newDistance;

                        do
                        {
                            oldDistance = Vector3.Distance(position, point);
                            var newAngle = positiveBool ? 5 : -5;

                            var newPos = currentPoint + (Quaternion.AngleAxis(angle + newAngle, halfDirection) * tan) * (width[i] / 2);
                            newDistance = Vector3.Distance(newPos, point);
                            //Debug.Log("Loop: " + i + " " + oldDistance + " " + newDistance);

                            if (newDistance < oldDistance)
                            {
                                angle += newAngle;
                            }

                            position = newPos;

                        } while (oldDistance > newDistance);
                    }

                    if (vc < lastVIndex)
                    {
                        position = currentPoint + (Quaternion.AngleAxis(((float)vc / (loopVertCount - 1) * 360f) + angle, halfDirection) * tan) * (width[i] / 2);
                        vertices[vert] = position;

                    }
                    else
                    {
                        vertices[vert] = vertices[vert - (lastVIndex)];   //Setting last vertex in loop to position of first vertex in loop
                    }

                    if (i == 0)
                    {
                        startVerts[vc] = vertices[vert];

                        var distance = Vector3.Distance(new Vector3(0, refPoints[0].y, 0), new Vector3(0, vertices[vc].y, 0));
                        Vector3 newOrigin = origin + (parentDirection * distance);
                        newOrigin.y = vertices[vc].y;
                        gizmoPositions.Add(newOrigin);

                        normals[vert] = (vertices[vc] - newOrigin).normalized;
                    }
                    else
                    {
                        normals[vert] = (vertices[vert] - currentPoint).normalized;
                    }

                    //Debug.Log(normals[vert]);
                    vert++;
                }

                if (i == 0 && blend)
                {
                    var dir = refPoints[1] - refPoints[0];


                    for (int n = 0; n < branchBlend; n++)
                    {
                        for (int vc = 0; vc < loopVertCount; vc++)
                        {
                            vertices[vert] = vertices[vc] + (dir / 2) * (((float)n + 1) / (branchBlend));

                            var distance = Vector3.Distance(new Vector3(0, refPoints[0].y, 0), new Vector3(0, vertices[vc].y, 0));
                            Vector3 newOrigin = origin + (parentDirection * distance);
                            newOrigin.y = vertices[vc].y;
                            gizmoPositions.Add(newOrigin);

                            //normals[vert] = Vector3.Lerp((vertices[vert] - newPoint).normalized,(vertices[vert] - currentPoint).normalized,(float)n/branchBlend);
                            var newNormal = ((vertices[vc] - newOrigin).normalized + (vertices[vert] - currentPoint).normalized).normalized;
                            //var newNormal = (vertices[vc] - newOrigin).normalized;

                            normals[vert] = Vector3.Lerp(newNormal, (vertices[vert] - currentPoint).normalized, (float)n / branchBlend);

                            vert++;
                        }
                    }
                }


                lastDir = direction;

            }
            else
            {
                vertices[vert] = refPoints[i];  //Generating End Point
                normals[vert] = (refPoints[i] - refPoints[i - 1]).normalized;
            }
        }
        return (vertices, normals, gizmoPositions.ToArray());
    }

    public static int[] GenerateBranchTriangles(int loopCuts, int loopVertCount, Vector3[] verts)
    {
        loopVertCount += 1;
        int vert = 0;
        List<int> triangles = new List<int>();

        for (int i = 0; i < loopCuts - 1; i++)
        {
            if (i < loopCuts - 2)
            {
                for (int qi = 0; qi < loopVertCount; qi++)
                {
                    if (qi < loopVertCount - 1)
                    {
                        triangles.Add(vert + loopVertCount + 1);
                        triangles.Add(vert + loopVertCount);
                        triangles.Add(vert + 1);
                        triangles.Add(vert + 1);
                        triangles.Add(vert + loopVertCount);
                        triangles.Add(vert);
                    }

                    vert++;
                }
            }
            else
            {

                for (int ti = 0; ti < loopVertCount; ti++) //Generating tip of mesh
                {
                    if (ti < loopVertCount - 1)
                    {
                        triangles.Add(vert + 1);
                        triangles.Add(verts.Length - 1);
                        triangles.Add(vert);
                    }

                    vert++;
                }

            }
        }

        return triangles.ToArray();
    }

    public static Vector2[] GenerateBranchUVs(int loopCuts, int loopVertCount, Vector3[] verts, bool mainBranch, int trunkLoopCuts = 0)
    {
        loopVertCount += 1;
        Vector2[] meshUV = new Vector2[verts.Length];
        int loops = ((verts.Length - 1) / loopVertCount) + 1;
        var vert = 0;
        float[] layerDist = new float[loopVertCount];

        for (int i = 0; i < loops; i++)
        {

            if (i < loops - 1)
            {
                for (int n = 0; n < loopVertCount; n++)
                {
                    if (i > 0)
                    {
                        layerDist[n] += Vector3.Distance(verts[vert], verts[vert - loopVertCount]) / 10;
                    }

                    if (i < trunkLoopCuts && mainBranch)
                    {
                        if (n < loopVertCount - 1)
                        {
                            meshUV[vert] = new Vector2((float)n / (loopVertCount - 1), layerDist[n]);
                        }
                        else
                        {
                            meshUV[vert] = new Vector2((float)1, layerDist[n]);
                        }
                    }
                    else
                    {
                        if (n < loopVertCount - 1)
                        {
                            meshUV[vert] = new Vector2((float)n / (loopVertCount - 1), layerDist[0]);
                        }
                        else
                        {
                            meshUV[vert] = new Vector2((float)1, layerDist[0]);
                        }
                    }

                    vert++;
                }
            }
            else
            {
                meshUV[vert] = new Vector2(0.5f, layerDist[0] + 0.5f);
            }
        }
        return meshUV;
    }

    public static Vector3[] BranchTwist(Transform obj, Vector3 objPos, int loopCuts, int loopVertCount, Vector3[] verts, Vector3[] refPoints, float treeTwist)
    {
        var vert = 0;
        for (int i = 0; i < loopCuts; i++)
        {
            //Checking if loop is on peak loop
            if (i < loopCuts - 1)
            {
                for (int vc = 0; vc < loopVertCount; vc++)
                {
                    if (i < 8)
                    {
                        verts[vert] = RotateVertexAroundAxis(verts[vert], refPoints[i] - objPos, Vector3.up, treeTwist * i);
                    }
                    else
                    {
                        verts[vert] = RotateVertexAroundAxis(verts[vert], refPoints[i] - objPos, refPoints[i + 1] - refPoints[i], treeTwist * i);
                    }

                    vert++;
                }
            }
        }
        return verts;
    }

    //  Methods for Vertex generation / editing
    public static Vector3 GeneratePositionOnCircle(Vector3 origin, Vector3 direction, int vertCount, float width, int vc, Vector3 tangent, Vector3 lastDir = default(Vector3))
    {
        var r = width / 2;

        //var position = origin + (Quaternion.AngleAxis(((float)vc/vertCount * 360f) + angle,direction) * tangent);


        var degrees = (360f / vertCount) * (vc);

        var x = origin.x + r * Mathf.Sin((degrees * Mathf.PI) / 180);
        var y = origin.y;
        var z = origin.z + r * Mathf.Cos((degrees * Mathf.PI) / 180);

        var position = new Vector3(x, y, z);

        position = Quaternion.FromToRotation(Vector3.up, direction) * (position - origin) + origin;

        return position;
    }

    public static Vector3 RotateVertexAroundPoint(Vector3 point, Vector3 pivot, Quaternion rotation)
    {
        return rotation * (point - pivot) + pivot;
    }

    public static Vector3 RotateVertexAroundAxis(Vector3 point, Vector3 origin, Vector3 axis, float angle)
    {
        var rot = Quaternion.AngleAxis(angle, axis);
        var dir = point - origin;
        dir = rot * dir;
        return origin + dir;
    }

    public static Vector3 GetCenter(Vector3[] verts, int loopVertCount, int currentLoop)
    {
        var startVertIndex = currentLoop * (loopVertCount + 1);
        var center = verts[startVertIndex] + (verts[startVertIndex + (loopVertCount / 2)] - verts[startVertIndex]) / 2;
        return center;
    }

    //  Tree Shape Methods
    public static void GenerateTruckVariables(System.Random random, float height, int loopCuts, int loopVertCount, ref float[] rootChance, ref int[] rootWidth, ref float[] rootLengthMult, ref float[] flareHeight)
    {

        //Generating all random numbers needed
        for (int n = 0; n < loopVertCount; n++)
        {
            float rootWidthVariation = RandomRange(random, 0, Mathf.FloorToInt(loopVertCount / 10));
            rootChance[n] = RandomRange(random, 0f, 1f);
            rootWidth[n] = 3 + (int)rootWidthVariation;// + Mathf.FloorToInt(RandomRange(random,0f,minRootVertCount));
            rootLengthMult[n] = RandomRange(random, 0.5f, 1f);
            flareHeight[n] = (height - RandomRange(random, 0, height / 5)) / loopCuts;
        }
    }

    public static Vector2[] TrunkGeneration(Vector3[] vertices, int trunkLoopCuts, int treeLoopCuts, int loopDetail, float height, float[] rootChance, int[] rootWidth, float[] rootLengthMult, float[] flareHeight, float treeWidth, AnimationCurve rootShape, AnimationCurve rootBlend, bool generateTrunkFlares)
    {
        var vert = 0;
        var vert2 = 0;
        var lastHeight = 0f;
        int rootVertCount = Mathf.FloorToInt(loopDetail / 10) < 2 ? 2 : Mathf.FloorToInt(loopDetail / 10);
        List<int> rootVerts = new List<int>();
        List<float> rootVertLengths = new List<float>();
        List<float> rootHeights = new List<float>();
        Vector3[] center = new Vector3[trunkLoopCuts];

        for (int i = 0; i < trunkLoopCuts; i++)
        {
            center[i] = vertices[i * (loopDetail + 1)] + (vertices[(i * (loopDetail + 1)) + (loopDetail / 2)] - vertices[i * (loopDetail + 1)]) / 2;

            //Trunk Height Loop
            for (int vc = 0; vc < loopDetail + 1; vc++, vert++)
            {
                if (vc < loopDetail)
                {
                    if (i == 0)
                    { // First base level of vertices to determine length, height and width

                        //Trunk Roots
                        if (1 > 1 - treeWidth / 7 && vc + rootWidth[vc] < loopDetail)
                        {
                            for (int n = 0; n < rootWidth[vc]; n++)
                            {
                                var newDir = new Vector3((vertices[vert + n] - center[i]).x, 0, (vertices[vert + n] - center[i]).z).normalized;
                                var rootLength = Mathf.Lerp(0.35f, (rootLengthMult[vc] * (treeWidth / 2)), rootShape.Evaluate((float)n / (rootWidth[vc] - 1)));

                                rootVerts.Add(vert + n);
                                rootVertLengths.Add(rootLength);
                                rootHeights.Add(flareHeight[vc]);
                            }
                            vert += rootWidth[vc] - 1;
                            vc += rootWidth[vc] - 1;

                        }
                    }
                    else if (i < trunkLoopCuts)
                    { // Tapering vertices back to tree width

                        if (rootVerts.Contains(vc))
                        { // if vertex is in a root
                            vertices[vert] = new Vector3(vertices[vert].x, rootHeights[rootVerts.IndexOf(vc)] * i, vertices[vert].z);
                            lastHeight = rootHeights[rootVerts.IndexOf(vc)];
                        }
                        else
                        {
                            if (lastHeight != 0)
                            {
                                vertices[vert] = new Vector3(vertices[vert].x, lastHeight * i, vertices[vert].z);
                            }
                        }
                    }
                }
                else
                {
                    vertices[vert] = vertices[vert - loopDetail];
                }
            }
        }

        var uvs = GenerateBranchUVs(treeLoopCuts, loopDetail, vertices, true, trunkLoopCuts);

        //Trunk Flare Loop
        for (int i = 0; i < trunkLoopCuts; i++)
        {
            if (generateTrunkFlares)
            {
                for (int vc = 0; vc < loopDetail + 1; vc++, vert2++)
                {
                    if (vc < loopDetail)
                    {
                        var newDir = new Vector3((vertices[vert2] - center[i]).x, 0, (vertices[vert2] - center[i]).z).normalized;

                        if (rootVerts.Contains(vc))
                        { // if vertex is in a root
                            vertices[vert2] += newDir * Mathf.Lerp(rootVertLengths[rootVerts.IndexOf(vc)], 0, rootBlend.Evaluate((float)i / trunkLoopCuts));
                        }
                        else
                        {
                            vertices[vert2] += newDir * Mathf.Lerp(0.35f, 0f, (float)i / trunkLoopCuts);
                        }

                    }
                    else
                    {
                        vertices[vert2] = vertices[vert2 - loopDetail];
                    }
                }
            }
        }

        return uvs;
    }

    public static (List<meshData[]> meshes, int lodLevel, int uniqueCode, branchStartVars branchStartVerts) MainBranchGeneration(GenVars genV, TreeVars treeV, TrunkVars trunkV, BranchVars branchV, SubBranchVars subBranchV, LeafVars leafV, RootVars rootV, System.Random random, int uniqueCode, int treeSeed, Transform obj, Vector3 objPos, Vector3 objUp, int lodLevel)
    {//,ref List<Vector3> treeVerts, ref List<int> treeTris, ref List<Vector2> treeUVs){
        var treeRandom = new System.Random(treeSeed);
        var branchRandom = new System.Random(treeSeed);
        var subBranchRandom = new System.Random(treeSeed);
        var leafRandom = new System.Random(treeSeed);

        var treeBaseCount = 1;//Mathf.RoundToInt( (float)(random.NextDouble() * (treeV.treeBaseCountSlide.y - treeV.treeBaseCountSlide.x) + treeV.treeBaseCountSlide.x) );

        //Debug.Log("LOD " + lodLevel + " generating with seed " + treeSeed);         

        //Tree Scale Values
        var treeWidth = treeV.treeWidth * treeV.scale;
        var treeHeight = treeV.treeHeight * treeV.scale;
        var branchLength = branchV.length * treeV.scale;
        var leafSize = leafV.size * treeV.scale;

        //Per tree Variable setup
        var height = treeHeight;//baseIndex > 0 ? treeHeight * Mathf.Clamp(1 + RandomRange(random,-treeV.treeBaseHeightRandomness,treeV.treeBaseHeightRandomness),0.25f,1.25f) : treeHeight;
        List<meshData[]> meshes = new List<meshData[]>();
        branchStartVars branchStartVerts = new branchStartVars();
        List<Vector3> treeVerts = new List<Vector3>();
        List<Vector3> treeNorms = new List<Vector3>();
        List<int> treeTris = new List<int>();
        List<Vector2> treeUVs = new List<Vector2>();

        //Trunk Generation Variables
        float[] flareChance = new float[treeV.treeLoopVertCount];
        int[] flareWidth = new int[treeV.treeLoopVertCount];
        float[] flareLengthMult = new float[treeV.treeLoopVertCount];
        float[] flareHeight = new float[treeV.treeLoopVertCount];
        var trunkActualHeight = height * trunkV.trunkHeight; //Trunk Height Relative to tree height

        //Leaf Generation Variables 
        var leafVerts = new List<Vector3>();
        var leafUVs = new List<Vector2>();
        var leafTris = new List<int>();

        var lodTreeLoopVertCount = lodLevel == 0 ? treeV.treeLoopVertCount : treeV.treeLoopVertCount - (int)((treeV.treeLoopVertCount / genV.lodLevels) * lodLevel);
        lodTreeLoopVertCount = lodTreeLoopVertCount < 3 ? 3 : lodTreeLoopVertCount;

        var treePointWidth = new List<float>();
        var newLoopCuts = genV.generateTrunk ? treeV.treeLoopCuts + trunkV.trunkLoopCuts + 1 : treeV.treeLoopCuts + 1;
        var treePoints = new Vector3[newLoopCuts];
        var initialDirection = objUp;//baseIndex == 0 ? objUp : (GeneratePositionOnCircle(objPos + objUp,objUp,36,treeWidth * treeV.treeBaseAngle,random.Next(1,36)) - objPos).normalized;
        bool botGen = true;

        treePoints[0] = Vector3.zero;//baseIndex == 0 ? objPos : GeneratePositionOnCircle(objPos,Vector3.up,12,(treeWidth*4)*treeV.treeBaseSpacing,random.Next(1,12)); //Setting origin of tree based off of multi-base properties
        treePointWidth.Add(treeWidth);



        for (int n = 1; n < newLoopCuts; n++)
        {
            if (genV.generateTrunk)
            {
                if (n < trunkV.trunkLoopCuts)
                {
                    treePointWidth.Add(treePointWidth[n - 1] - (((treeWidth * treeV.treeTaper) / trunkV.trunkLoopCuts) * trunkV.trunkHeight));
                    if (n == 1)
                    {
                        treePoints[n] = treePoints[n - 1] + (initialDirection * ((height * trunkV.trunkHeight) / trunkV.trunkLoopCuts));
                    }
                    else
                    {
                        treePoints[n] = treePoints[n - 1] + ((treePoints[n - 1] - treePoints[n - 2]).normalized * ((height * trunkV.trunkHeight) / trunkV.trunkLoopCuts));
                    }
                }
                else
                {
                    treePointWidth.Add(treePointWidth[n - 1] - (((treeWidth - (treeWidth * trunkV.trunkHeight)) * treeV.treeTaper) / treeV.treeLoopCuts));
                    botGen = false;
                }

            }
            else
            {
                treePointWidth.Add(treePointWidth[n - 1] - ((treeWidth * treeV.treeTaper) / treeV.treeLoopCuts));
                if (n > 0 && n < 2)
                {
                    treePoints[n] = treePoints[n - 1] + (initialDirection * ((height / (newLoopCuts - 1)) * n));
                }
                else if (n > 0)
                {
                    botGen = false;
                }
            }

            if (n < newLoopCuts - 1 && !botGen)
            {
                var newPointPos = treePoints[n - 1] + ((treePoints[n - 1] - treePoints[n - 2]).normalized * ((height - (height * trunkV.trunkHeight)) / treeV.treeLoopCuts));
                var direction = (treePoints[n] - treePoints[n - 1]).normalized;
                Vector3 tangent = Vector3.Cross(direction, (direction == Vector3.up) ? Vector3.forward : Vector3.up).normalized;
                var treeStraightnessShift = (GeneratePositionOnCircle(treePoints[n], direction, 100, treeV.treeStraightness * ((treeHeight - (treeHeight * trunkV.trunkHeight)) / 200), treeRandom.Next(0, 99), tangent) - treePoints[n]);
                treePoints[n] = newPointPos + treeStraightnessShift;
            }
            else if (n == newLoopCuts - 1 && !botGen)
            {
                treePoints[n] = treePoints[n - 1] + ((treePoints[n - 1] - treePoints[n - 2]) * treeV.treePointSharpness);
            }
        }

        var vertCount = treeVerts.Count;
        var tempVerts = GenerateBranchVertices(obj, newLoopCuts, lodTreeLoopVertCount, treePointWidth.ToArray(), true, treePoints, treeV.treeTaper, false);
        var tempTris = GenerateBranchTriangles(newLoopCuts, lodTreeLoopVertCount, tempVerts.verts);
        Vector2[] tempUVs = null;

        //**TRUNK GENERATION**//
        if (genV.generateTrunk)
        {
            GenerateTruckVariables(random, trunkActualHeight, trunkV.trunkLoopCuts, treeV.treeLoopVertCount, ref flareChance, ref flareWidth, ref flareLengthMult, ref flareHeight);
            tempUVs = TrunkGeneration(tempVerts.verts, trunkV.trunkLoopCuts, treeV.treeLoopCuts, lodTreeLoopVertCount, trunkActualHeight, flareChance, flareWidth, flareLengthMult, flareHeight, treeV.treeWidth, trunkV.rootShape, trunkV.rootBlend, genV.generateTrunkFlares);
        }
        else
        {
            tempUVs = GenerateBranchUVs(newLoopCuts, lodTreeLoopVertCount, tempVerts.verts, true, trunkV.trunkLoopCuts);
        }

        tempVerts.verts = BranchTwist(obj, objPos, newLoopCuts, lodTreeLoopVertCount + 1, tempVerts.verts, treePoints, treeV.treeTwist);// Applying twist after trunk generates

        for (int i = 0; i < tempTris.Length; i++)
        {
            if (i < tempVerts.verts.Length)
            {
                treeUVs.Add(tempUVs[i]);
                treeVerts.Add(tempVerts.verts[i]);
                treeNorms.Add(tempVerts.normals[i]);
            }
            treeTris.Add(tempTris[i] + vertCount);
        }

        meshData[] treeMeshData = new meshData[1] { new meshData() };  //Initialize as array to support > 64k vertex meshes
        treeMeshData[0].verts = treeVerts.ToArray();
        treeMeshData[0].tris = treeTris.ToArray();
        treeMeshData[0].uvs = treeUVs.ToArray();
        treeMeshData[0].norms = treeNorms.ToArray();
        meshes.Add(treeMeshData);
        //meshes[0] = (treeVerts.ToArray(),treeTris.ToArray(), treeUVs.ToArray());

        //**BRANCH GENERATION**//
        if (genV.generateBranches)
        {
            int branchGenStart = genV.generateTrunk ? trunkV.trunkLoopCuts - 1 : 0;
            var meshData = GenerateBranches(branchV, subBranchV, leafV, branchRandom, subBranchRandom, leafRandom, treeV.scale, obj, objPos, treePoints, treePointWidth.ToArray(), branchGenStart, genV.generateSubBranches, genV.lodLevels, lodLevel);
            branchStartVerts = meshData.branchStartVerts;
            meshData[] branchData = new meshData[meshData.branchMeshData.Length];
            for (int i = 0; i < branchData.Length; i++)
            {
                branchData[i] = new meshData();
            }

            meshData[] leafData = new meshData[meshData.leafMeshData.Length];
            for (int i = 0; i < leafData.Length; i++)
            {
                leafData[i] = new meshData();
            }

            for (int i = 0; i < branchData.Length; i++)
            {
                branchData[i].verts = meshData.branchMeshData[i].verts.ToArray();
                branchData[i].tris = meshData.branchMeshData[i].tris.ToArray();
                branchData[i].uvs = meshData.branchMeshData[i].uvs.ToArray();
                branchData[i].norms = meshData.branchMeshData[i].norms.ToArray();
            }

            for (int i = 0; i < leafData.Length; i++)
            {
                leafData[i].verts = meshData.leafMeshData[i].verts.ToArray();
                leafData[i].tris = meshData.leafMeshData[i].tris.ToArray();
                leafData[i].uvs = meshData.leafMeshData[i].uvs.ToArray();
                leafData[i].norms = meshData.leafMeshData[i].norms.ToArray();
            }

            meshes.Add(branchData);
            meshes.Add(leafData);
        }

        //**ROOT GENERATION**//
        /*
        if(genV.generateRoots){
            meshes[3] = GenerateRoots(random, obj, objPos, treePoints, treePointWidth.ToArray(), treeBaseCount, trunkV.trunkLoopCuts, rootV.loopCuts, rootV.loopVertCount, rootV.rootGenPos, rootV.length, rootV.rootDensity,
            rootV.width,rootV.rootInitialAngle,rootV.taper,rootV.rootStraightness,rootV.rootDroop,rootV.endSharpness,rootV.splitChance,rootV.subDensity, rootV.subLength, rootV.subDirectionAlignment, branchV.valueCurve, genV.generateSubRoots,lodLevel, genV.lodLevels);
        }*/

        return (meshes, lodLevel, uniqueCode, branchStartVerts);

    }

    //GROUP OBJECT GENERATION METHODS
    public static (meshDataList[] branchMeshData, branchStartVars branchStartVerts, meshDataList[] leafMeshData) GenerateBranches(BranchVars bV, SubBranchVars sbV, LeafVars lV, System.Random bRandom, System.Random sbRandom, System.Random lRandom, float treeScale, Transform parent, Vector3 objPos, Vector3[] treePoints, float[] treePointWidth, int branchGenStart, bool generateSubBranches, int lodLevels, int lodLevel)
    {
        //Generating variables per LOD
        var loopVertCount = lodLevel == 0 ? bV.loopVertCount : bV.loopVertCount - (int)((bV.loopVertCount / lodLevels) * lodLevel);
        loopVertCount = loopVertCount < 3 ? 3 : loopVertCount;
        var branchPoints = new List<Vector3>();

        //Generating Empty lists for new mesh data
        List<meshDataList> branchMeshData = new List<meshDataList>();
        branchStartVars branchStartVerts = new branchStartVars();
        branchMeshData.Add(new meshDataList());
        //(List<Vector3> verts,List<int> tris,List<Vector2> uvs) leafMeshData = (new List<Vector3>(),new List<int>(),new List<Vector2>());
        List<meshDataList> leafMeshData = new List<meshDataList>();
        leafMeshData.Add(new meshDataList());

        List<(Vector3 origin, Vector3 direction, float length, float width)> subBranchVars = new List<(Vector3, Vector3, float, float)>();

        //Generating branches
        for (int i = branchGenStart + Mathf.FloorToInt((treePoints.Length - branchGenStart) * bV.genStartPosition); i < treePoints.Length - 3; i++)
        {
            for (int b = 0; b < bV.density; b++)
            {
                var leaves = lV.leafGenPos.x == 0 ? true : false;
                var branchRatio = ((float)i - branchGenStart) / (treePoints.Length - branchGenStart);
                var treeDirection = treePoints[i + 1] - treePoints[i];
                Vector3 tangent = Vector3.Cross(treeDirection, (treeDirection == Vector3.up) ? Vector3.forward : Vector3.up).normalized;
                var origin = treePoints[i] + ((treePoints[i + 1] - treePoints[i]) * RandomRange(bRandom, 0f, 1f));
                var randomPoint = GeneratePositionOnCircle(origin, treePoints[i + 1] - treePoints[i], 100, 1, bRandom.Next(0, 99), tangent);
                var flatDirection = (randomPoint - origin).normalized;
                var direction = (flatDirection + ((treePoints[i + 1] - treePoints[i]).normalized * Mathf.Clamp(bV.initialAngle + Mathf.Lerp(0, 1, bV.heightRelativeAngle.Evaluate(branchRatio)), -1, 1))).normalized;
                var width = (treePointWidth[i] / (2 - bV.heightRelativeWidth));
                var length = ((bV.length * treeScale) * (1 - Mathf.Lerp(0.95f, 0f, bV.heightRelativeLength.Evaluate(branchRatio)))) / (bV.loopCuts - 1);

                var newMeshData = GenerateBranch(bV, sbV, lV, bRandom, sbRandom, lRandom, treeScale, parent, objPos, origin, direction, length, width, true, leaves, ref subBranchVars, flatDirection, treeDirection.normalized);
                branchStartVerts.branchDirections.Add(direction);

                foreach (var item in newMeshData.branchMeshData.gizmoPositions)
                {
                    branchStartVerts.gizmoPositions.Add(item);
                }

                for (int x = 0; x < branchMeshData.Count; x++)  //For loop adds another item if mesh vert count is over threshold
                {
                    if (!(branchMeshData[x].verts.Count + newMeshData.branchMeshData.verts.Length > 65535))
                    {
                        branchStartVerts.startVerts.Add(new int[bV.loopVertCount + 1]);
                        for (int n = 0; n < bV.loopVertCount + 1; n++)
                        {
                            branchStartVerts.startVerts[branchStartVerts.startVerts.Count - 1][n] = n + branchMeshData[x].verts.Count;
                        }

                        AddMeshData(branchMeshData[x], newMeshData.branchMeshData);
                    }
                    else if (x == branchMeshData.Count - 1)
                    {
                        branchMeshData.Add(new meshDataList());
                    }
                }

                for (int x = 0; x < leafMeshData.Count; x++)
                {
                    if (!(leafMeshData[x].verts.Count + newMeshData.leafMeshData.verts.Length > 65535))
                    {
                        AddMeshData(leafMeshData[x], newMeshData.leafMeshData);
                    }
                    else if (x == leafMeshData.Count - 1)
                    {
                        leafMeshData.Add(new meshDataList());
                    }
                }
            }
        }

        //SUB BRANCH GENERATION

        if (generateSubBranches)
        {
            for (int n = 0; n < sbV.genSteps; n++)
            {
                var subLoopVerCount = n == 0 ? 3 : 2;
                var split = n < sbV.genSteps - 1 ? true : false;
                var leaves = n + 1 >= lV.leafGenPos.x && n + 1 <= lV.leafGenPos.y ? true : false;
                var tempVars = subBranchVars;
                subBranchVars = new List<(Vector3 origin, Vector3 direction, float length, float width)>();

                for (int i = 0; i < tempVars.Count; i++)
                {
                    var newMeshData = GenerateBranch(bV, sbV, lV, sbRandom, sbRandom, lRandom, treeScale, parent, objPos, tempVars[i].origin, tempVars[i].direction, tempVars[i].length, tempVars[i].width, split, leaves, ref subBranchVars);

                    for (int x = 0; x < branchMeshData.Count; x++)
                    {
                        if (!(branchMeshData[x].verts.Count + newMeshData.branchMeshData.verts.Length > 65535))
                        {
                            AddMeshData(branchMeshData[x], newMeshData.branchMeshData);
                        }
                        else if (x == branchMeshData.Count - 1)
                        {
                            branchMeshData.Add(new meshDataList());
                        }
                    }

                    for (int x = 0; x < leafMeshData.Count; x++)
                    {
                        if (!(leafMeshData[x].verts.Count + newMeshData.leafMeshData.verts.Length > 65535))
                        {
                            AddMeshData(leafMeshData[x], newMeshData.leafMeshData);
                        }
                        else if (x == leafMeshData.Count - 1)
                        {
                            leafMeshData.Add(new meshDataList());
                        }
                    }
                }
            }
        }

        return (branchMeshData.ToArray(), branchStartVerts, leafMeshData.ToArray());

    }

    private static (Vector3[], int[], Vector2[]) GenerateLeaves(Transform parent, Vector3[] leafVerts, int[] leafTris, Vector2[] leafUVs)
    {

        return (leafVerts, leafTris, leafUVs);
    }

    private static (Vector3[], int[], Vector2[]) GenerateRoots(System.Random random, Transform parent, Vector3 objPos, Vector3[] treePoints, float[] treePointWidth, int treeBaseCount, int trunkLoopCuts, int rootLoopCuts, int rootLoopVertCount, Vector2 rootHeight, float rootLength, int rootDensity, float rootWidth, float rootInitialAngle, float taper, float straightness, float droop, float endSharpness, float splitChance, int subDensity, float subLength, float subDirectionAlignment, AnimationCurve branchValueCurve, bool generateSubRoots, int lodLevel, int lodLevels)
    {
        //Generating variables per LOD
        var loopVertCount = lodLevel == 0 ? rootLoopVertCount : rootLoopVertCount - (int)((rootLoopVertCount / lodLevels) * lodLevel);
        loopVertCount = loopVertCount < 3 ? 3 : loopVertCount;
        List<Vector3> rootVerts = new List<Vector3>();
        List<int> rootTris = new List<int>();
        List<Vector2> rootUVs = new List<Vector2>();
        List<(Vector3 origin, Vector3 direction, float length, float width)> subRootVars = new List<(Vector3, Vector3, float, float)>();

        for (int i = Mathf.RoundToInt(rootHeight.x * trunkLoopCuts); i < Mathf.RoundToInt(rootHeight.y * trunkLoopCuts); i++)
        {
            for (int b = 0; b < Mathf.Ceil((float)rootDensity / (float)treeBaseCount); b++)
            {
                var origin = treePoints[i] + (treePoints[i + 1] - treePoints[i]) * RandomRange(random, -1f, 1f);
                var treeDirection = treePoints[i + 1] - treePoints[i];
                Vector3 tangent = Vector3.Cross(treeDirection, (treeDirection == Vector3.up) ? Vector3.forward : Vector3.up).normalized;
                var randomPoint = GeneratePositionOnCircle(origin, treePoints[i + 1] - treePoints[i], 100, 1, random.Next(0, 99), tangent);
                var direction = (randomPoint - origin) + ((treePoints[i + 1] - treePoints[i]).normalized * rootInitialAngle);
                var width = treePointWidth[i] / (2 - rootWidth);
                var length = (rootLength) / (rootLoopCuts - 1);

                GenerateRoot(random, parent, objPos, origin, direction, length, width, rootLoopCuts, loopVertCount, taper, straightness, droop, endSharpness, true, splitChance, subDensity, subLength, subDirectionAlignment, branchValueCurve, ref rootVerts, ref rootTris, ref rootUVs, ref subRootVars);
            }
        }

        if (generateSubRoots)
        {
            for (int i = 0; i < subRootVars.Count; i++)
            {
                GenerateRoot(random, parent, objPos, subRootVars[i].origin, subRootVars[i].direction, subRootVars[i].length, subRootVars[i].width, rootLoopCuts, loopVertCount, taper, straightness, droop, endSharpness, true, splitChance, subDensity, subLength, subDirectionAlignment, branchValueCurve, ref rootVerts, ref rootTris, ref rootUVs, ref subRootVars);
            }
        }

        return (rootVerts.ToArray(), rootTris.ToArray(), rootUVs.ToArray());
    }

    //INDIVIDUAL OBJECT GENERATION METHODS
    private static (meshData branchMeshData, meshData leafMeshData) GenerateBranch(BranchVars bV, SubBranchVars sbV, LeafVars lV, System.Random bRandom, System.Random sbRandom, System.Random lRandom, float treeScale, Transform parent, Vector3 objPos, Vector3 origin, Vector3 direction, float length, float width, bool split, bool genLeaves, ref List<(Vector3, Vector3, float, float)> subBranchVars, Vector3 flatDirection = default(Vector3), Vector3 parentDirection = default(Vector3))
    {
        var loops = bV.loopCuts + 1; // Adding one to make account for peak and make loopcuts actually the variable that user set
        List<float> pointWidth = new List<float>();
        pointWidth.Add(width);
        Vector3[] pointsPos = new Vector3[loops];
        Vector3 newDirection = Vector3.zero;

        //Generating initial branch points
        for (int i = 0; i < loops; i++)
        {
            if (i > 0)
                pointWidth.Add(pointWidth[i - 1] - (width / loops) * bV.taper);

            if (i < 2)
            {
                pointsPos[i] = origin + (flatDirection * pointWidth[i]) + (direction * length * i);
            }
            else
            {
                newDirection = (pointsPos[i - 1] - pointsPos[i - 2]).normalized;

                if (i < loops - 1)
                {
                    pointsPos[i] = pointsPos[i - 1] + (newDirection * length);
                    Vector3 tangent = Vector3.Cross(newDirection, (newDirection == Vector3.up) ? Vector3.forward : Vector3.up).normalized;
                    pointsPos[i] += new Vector3(0, Mathf.Lerp(0f, length * bV.droop, bV.valueCurve.Evaluate((float)i / (float)loops) - bV.rigidity), 0);
                    pointsPos[i] += GeneratePositionOnCircle(pointsPos[i], newDirection, 100, bV.straightness * length, bRandom.Next(0, 99), tangent) - pointsPos[i];
                    //pointsPos[i] += (pointsPos[0] - pointsPos[loops-2]) * bV.crinkliness * length;
                }
                else
                {
                    pointsPos[i] = pointsPos[i - 1] + (newDirection * length) * bV.endSharpness;
                }
            }
        }

        for (int i = 0; i < loops; i++)
        {

            if (i > 0)
            {
                if (i < loops - 1)
                {
                    pointsPos[i] += ((pointsPos[0] - pointsPos[loops - 1]).normalized * bV.crinkliness * ((float)i / (loops - 1)));//*Vector3.Distance(pointsPos[i],pointsPos[0]);//((float)i/(loops-1));
                }
                else
                {
                    newDirection = (pointsPos[i - 1] - pointsPos[i - 2]).normalized;
                    pointsPos[i] = pointsPos[i - 1] + (newDirection * length) * bV.endSharpness;
                }
            }
        }


        /*
        //Applying Branch Curl
        for (int i = loops-1; i > loops - ( loops * Mathf.Abs(branchCurl) ); i--)
        {
            var fromDirection = (pointsPos[i] - pointsPos[i-1]).normalized;
            var up = (branchDir.transform.up*branchCurl).normalized;
            var toDirection = up + (((pointsPos[i] - pointsPos[i-1]).normalized - up)*(0.9f*branchCurlScale));

            pointsPos[i] = RotateVertexAroundPoint(pointsPos[i],pointsPos[i-1],Quaternion.FromToRotation(fromDirection, toDirection) );
            for (int p = i+1; p < loops; p++)
            {
                pointsPos[p] = RotateVertexAroundPoint(pointsPos[p],pointsPos[i-1],Quaternion.FromToRotation(fromDirection, toDirection));
                pointsPos[p] += ( ( (pointsPos[p-1] - pointsPos[p]) ) / 2 ) * ((float)p/loops);

            }
        }*/

        //Generating Leaves
        meshDataList leafMeshDataList = new meshDataList();

        for (int i = Mathf.RoundToInt(pointsPos.Length * lV.genStartPos); i < pointsPos.Length - 1; i++)
        {
            for (int n = 0; n < lV.density; n++)
            {
                var leafRoll = RandomRange(lRandom, 0f, 360f * lV.randomRoll);
                var branchDirection = pointsPos[i + 1] - pointsPos[i];
                Vector3 tangent = Vector3.Cross(branchDirection, (branchDirection == Vector3.up) ? Vector3.forward : Vector3.up).normalized;
                var randomDirection = pointsPos[i] + (GeneratePositionOnCircle(pointsPos[i], branchDirection, 100, 1, lRandom.Next(0, 100), tangent) - pointsPos[i]).normalized;
                var leafDir = (randomDirection + ((pointsPos[i + 1] - randomDirection) * lV.branchDirectionAlignment)) - pointsPos[i];
                var leafPos = pointsPos[i] + ((branchDirection / lV.density) * n);
                var leafSize = (lV.size + (RandomRange(lRandom, -lV.size + 0.1f, lV.size) * lV.sizeRandomness)) * treeScale;

                if (genLeaves)
                {
                    var newMeshData = GenerateLeaf(leafPos, leafSize, leafRoll, leafDir);
                    AddMeshData(leafMeshDataList, newMeshData);
                }
            }
        }


        //Generating Split Branch Variables
        if (split)
        {
            var endLoop = loops - 2;
            for (int i = 1; i < endLoop; i++)
            {
                for (int n = 0; n < sbV.density; n++)
                {
                    if (RandomRange(sbRandom, 0f, 1f) < bV.splitChance)
                    {
                        var randomRatio = RandomRange(sbRandom, 0f, 1f);
                        var subOrigin = pointsPos[i] + ((pointsPos[i + 1] - pointsPos[i]) * randomRatio);
                        var branchDirection = pointsPos[i + 1] - pointsPos[i];
                        Vector3 tangent = Vector3.Cross(branchDirection, (branchDirection == Vector3.up) ? Vector3.forward : Vector3.up).normalized;
                        var randomPoint = GeneratePositionOnCircle(pointsPos[i], branchDirection, 100, 1, sbRandom.Next(0, 99), tangent);
                        var splitDirection = Vector3.Lerp((randomPoint - pointsPos[i]).normalized, (pointsPos[i + 1] - pointsPos[i]).normalized, sbV.branchDirectionAlignment);
                        var subLength = (length * sbV.length) * (1 - Mathf.Lerp(0, 0.95f, sbV.heightRelativeLength.Evaluate((float)i / endLoop)));
                        subBranchVars.Add((subOrigin, splitDirection, subLength, (Mathf.Lerp((pointWidth[i] / 1.5f), pointWidth[i + 1] / 1.5f, randomRatio)) * (0.1f + sbV.width)));
                    }
                }
            }
        }

        var tempVerts = GenerateBranchVertices(parent, loops, bV.loopVertCount, pointWidth.ToArray(), false, pointsPos, bV.taper, bV.weld, origin, parentDirection);
        var tempTris = GenerateBranchTriangles(loops, bV.loopVertCount, tempVerts.verts);
        var tempUVs = GenerateBranchUVs(loops, bV.loopVertCount, tempVerts.verts, false);
        meshData branchMeshData = new meshData();
        branchMeshData.verts = tempVerts.verts;
        branchMeshData.tris = tempTris;
        branchMeshData.uvs = tempUVs;
        branchMeshData.norms = tempVerts.normals;
        branchMeshData.gizmoPositions = tempVerts.gizmoPositions;


        meshData leafMeshData = new meshData();
        leafMeshData.verts = leafMeshDataList.verts.ToArray();
        leafMeshData.tris = leafMeshDataList.tris.ToArray();
        leafMeshData.uvs = leafMeshDataList.uvs.ToArray();

        return (branchMeshData, leafMeshData);
    }

    public static meshDataList AddMeshData(meshDataList mainMesh, meshData tempMesh)
    {
        var vertCount = mainMesh.verts.Count;
        var newMesh = mainMesh;

        for (int i = 0; i < tempMesh.tris.Length; i++)
        {
            if (i < tempMesh.verts.Length)
            {
                newMesh.uvs.Add(tempMesh.uvs[i]);
                newMesh.verts.Add(tempMesh.verts[i]);

                if (tempMesh.norms != null)
                    newMesh.norms.Add(tempMesh.norms[i]);
            }
            newMesh.tris.Add(tempMesh.tris[i] + vertCount);
        }

        return newMesh;
    }

    private static meshData GenerateLeaf(Vector3 position, float leafSize, float randomRoll, Vector3 leafDirection)
    {

        Vector3[] tempVerts = new Vector3[8];
        Vector2[] tempUV = new Vector2[8];
        int[] tempTris = new int[12];


        tempVerts[0] = position - ((Vector3.right / 2) * leafSize);
        tempVerts[1] = position + ((Vector3.right / 2) * leafSize);
        tempVerts[2] = (position - ((Vector3.right / 2) * leafSize)) + (Vector3.up * leafSize);
        tempVerts[3] = position + ((Vector3.right / 2) * leafSize) + (Vector3.up * leafSize);
        //Generating duplicate vertices for 2 sided plane
        tempVerts[4] = tempVerts[0];
        tempVerts[5] = tempVerts[1];
        tempVerts[6] = tempVerts[2];
        tempVerts[7] = tempVerts[3];

        tempUV[0] = new Vector2(0, 0);
        tempUV[1] = new Vector2(1, 0);
        tempUV[2] = new Vector2(0, 1);
        tempUV[3] = new Vector2(1, 1);
        tempUV[4] = new Vector2(0, 0);
        tempUV[5] = new Vector2(1, 0);
        tempUV[6] = new Vector2(0, 1);
        tempUV[7] = new Vector2(1, 1);

        tempTris[0] = 3;
        tempTris[1] = 2;
        tempTris[2] = 1;
        tempTris[3] = 1;
        tempTris[4] = 2;
        tempTris[5] = 0;
        tempTris[6] = 5;
        tempTris[7] = 6;
        tempTris[8] = 7;
        tempTris[9] = 4;
        tempTris[10] = 6;
        tempTris[11] = 5;

        for (int v = 0; v < tempVerts.Length; v++)
        {
            tempVerts[v] = RotateVertexAroundPoint(tempVerts[v], position, Quaternion.FromToRotation(Vector3.up, leafDirection));
            tempVerts[v] = RotateVertexAroundAxis(tempVerts[v], position, leafDirection, randomRoll);
        }
        meshData leafMeshData = new meshData();
        leafMeshData.verts = tempVerts;
        leafMeshData.tris = tempTris;
        leafMeshData.uvs = tempUV;

        return leafMeshData;
    }

    private static void GenerateRoot(System.Random random, Transform parent, Vector3 objPos, Vector3 origin, Vector3 direction, float length, float width, int loops, int loopVertCount, float taper, float straightness, float droop, float endSharpness, bool split, float splitChance, int subDensity, float subLength, float subDirectionAlignment, AnimationCurve branchValueCurve, ref List<Vector3> verts, ref List<int> tris, ref List<Vector2> UVs, ref List<(Vector3, Vector3, float, float)> subBranchVars)
    {
        loops += 1; // Adding one to make account for peak and make loopcuts actually the variable that user set
        List<float> pointWidth = new List<float>();
        pointWidth.Add(width);
        Vector3[] pointsPos = new Vector3[loops];
        Vector3 newDirection = Vector3.zero;

        //Generating initial branch points
        for (int i = 0; i < loops; i++)
        {
            if (i > 0)
                pointWidth.Add(pointWidth[i - 1] - (width / loops) * taper);

            if (i < 2)
            {
                pointsPos[i] = origin + (direction * length * i);
            }
            else
            {
                newDirection = (pointsPos[i - 1] - pointsPos[i - 2]).normalized;

                if (i < loops - 1)
                {
                    pointsPos[i] = pointsPos[i - 1] + (newDirection * length);
                    Vector3 tangent = Vector3.Cross(newDirection, (newDirection == Vector3.up) ? Vector3.forward : Vector3.up).normalized;

                    pointsPos[i] += new Vector3(0, Mathf.Lerp(0f, length * droop, branchValueCurve.Evaluate((float)i / (loops))), 0) +
                        (GeneratePositionOnCircle(pointsPos[i], newDirection, 100, straightness, random.Next(0, 99), tangent) - pointsPos[i]);
                }
                else
                {
                    pointsPos[i] = pointsPos[i - 1] + (newDirection * length) * endSharpness;
                }
            }
        }

        //Generating Split Branch Variables
        if (split)
        {
            var endLoop = loops - 3;
            for (int i = 0; i < endLoop; i++)
            {
                for (int n = 0; n < subDensity; n++)
                    if (i > 0 && RandomRange(random, 0f, 1f) < splitChance)
                    {
                        var subOrigin = pointsPos[i] + ((pointsPos[i + 1] - pointsPos[i]) * RandomRange(random, 0f, 1f));
                        var mainDirection = pointsPos[i + 1] - pointsPos[i];
                        Vector3 tangent = Vector3.Cross(mainDirection, (mainDirection == Vector3.up) ? Vector3.forward : Vector3.up).normalized;
                        var randomPoint = GeneratePositionOnCircle(pointsPos[i], mainDirection, 100, 1, random.Next(0, 99), tangent);
                        var splitDirection = Vector3.Lerp((randomPoint - pointsPos[i]).normalized, (pointsPos[i + 1] - pointsPos[i]).normalized, subDirectionAlignment);
                        var sLength = (length * subLength);
                        subBranchVars.Add((pointsPos[i], splitDirection, subLength, (width / 1.25f) / i));
                    }
            }
        }

        var vertCount = verts.Count;
        var tempVerts = GenerateBranchVertices(parent, loops, loopVertCount, pointWidth.ToArray(), false, pointsPos, taper, true);
        var tempTris = GenerateBranchTriangles(loops, loopVertCount, tempVerts.verts);
        var tempUVs = GenerateBranchUVs(loops, loopVertCount, tempVerts.verts, false);

        for (int i = 0; i < tempTris.Length; i++)
        {
            if (i < tempVerts.verts.Length)
            {
                UVs.Add(tempUVs[i]);
                verts.Add(tempVerts.verts[i]);
            }
            tris.Add(tempTris[i] + vertCount);
        }
    }
}